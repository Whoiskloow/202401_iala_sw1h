#include <iostream>
#include <random>

using namespace std;

int* genArr(int& tamano) {
    mt19937 gen(random_device{}());
    uniform_int_distribution<int> distribucionTamano(100, 500);
    tamano = distribucionTamano(gen);

    int* arreglo = new int[tamano];

    uniform_int_distribution<int> distribucionElementos(1, 10000);
    for (int i = 0; i < tamano; ++i) {
        arreglo[i] = distribucionElementos(gen);
    }

    return arreglo;
}

int main() {
    int tamano;
    int* arreglo = genArr(tamano);
    cout << "Tama�o del arreglo: " << tamano << endl;
    cout << "Arreglo generado:" << endl;
    for (int i = 0; i < tamano; ++i) {
        cout << arreglo[i] << " ";
    }
    cout << endl;

    delete[] arreglo;

    return 0;
}